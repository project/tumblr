<?php

/**
 * @file
 * Contains the Tumblr view row style plugin.
 */

/**
 * Plugin which performs a Tumblr view on the resulting object.
 *
 * Most of the code on this object is in the theme function.
 *
 * @ingroup views_row_plugins
 */
class tumblr_plugin_row_post_view extends views_plugin_row {
  // Basic properties that let the row style follow relationships.
  var $base_table = 'tumblr_post';
  var $base_field = 'id';

  // Stores the posts loaded with pre_render.
  var $posts = array();


  function render($row) {

    // Validate required fields.
    if (!isset($row->tumblr_post_type) || empty($row->tumblr_post_type)
      || !isset($row->tumblr_post_id) || empty($row->tumblr_post_id)) {

      return '';
    }

    // Get post.
    module_load_include('inc', 'tumblr');
    $theme_type = '';
    $post = tumblr_post_load(NULL, $row->tumblr_post_type, $row->tumblr_post_id);
    $post = is_array($post) ? array_shift($post) : $post;

    // Use theme functions of this module.
    switch ($post->type) {
      case 'text':
        $theme_type = 'tumblr_post_text';
        break;

      case 'quote':
        $theme_type = 'tumblr_post_quote';
        break;

      case 'link':
        $theme_type = 'tumblr_post_link';
        break;

      case 'answer':
        $theme_type = 'tumblr_post_answer';
        break;

      case 'chat':
        $theme_type = 'tumblr_post_chat';
        break;

      case 'video':
        $theme_type = 'tumblr_post_video';
        break;

      case 'audio':
        $theme_type = 'tumblr_post_audio';
        break;

      case 'photo':
        $theme_type = 'tumblr_post_photo';
        break;
    }

    return theme($theme_type, array('post' => $post));
  }
}
