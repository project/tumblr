<?php

/**
 * @file
 * Classes to implement the full Tumblr API
 */

/**
 * Class TumblrConfig
 *
 * Singleton which stores common configuration
 * @see http://php.net/manual/en/language.oop5.patterns.php
 */
class TumblrConf {

  private static $instance;
  private $attributes = array(
    'host' => 'www.tumblr.com',
    'api' => 'api.tumblr.com/v2',
  );

  private function __construct() {

  }

  public static function instance() {
    if (!isset(self::$instance)) {
      $className = __CLASS__;
      self::$instance = new $className;
    }
    return self::$instance;
  }

  /**
   * Generic getter
   *
   * @param $attribute
   *   string attribute name to return
   * @return
   *   mixed value or NULL
   */
  public function get($attribute) {
    if (array_key_exists($attribute, $this->attributes)) {
      return $this->attributes[$attribute];
    }
  }

  /**
   * Generic setter
   * @param $attribute
   *   string attribute name to be set
   * @param $value
   *   mixed value
   */
  public function set($attribute, $value) {
    if (array_key_exists($attribute, $this->attributes)) {
      $this->attributes[$attribute] = $value;
    }
  }

}

/**
 * Exception handling class.
 */
class TumblrException extends Exception {

}

/**
 * Primary Tumblr API implementation class
 * Supports the full REST API for tumblr.
 */
class Tumblr {

  /**
   * @var $format API format to use: can be json or xml
   */
  protected $format = 'json';

  /**
   * @var $source the tumblr api 'source'
   */
  protected $source = 'drupal';
  protected $username;
  protected $password;
  private $apikey;

  /**
   * Constructor for the Tumblr class
   */
  public function __construct($blog_name, $api_key) {
    if (!empty($blog_name) && !empty($api_key)) {
      $this->set_auth($blog_name, $api_key);
    }
  }

  /**
   * Set the username and password
   */
  public function set_auth($blog_name, $api_key) {
    $this->blog_name = $blog_name;
    $this->api_key = $api_key;
  }

  /**
   * Get an array of TumblrStatus objects from an API endpoint
   */
  protected function get_statuses($path, $params = array(), $use_auth = FALSE) {
    $values = $this->call($path, $params, 'GET', $use_auth);
    // Check on successfull call
    if ($values) {
      $statuses = array();
      foreach ($values as $status) {
        $statuses[] = new TumblrStatus($status);
      }
      return $statuses;
    }
    // Call might return FALSE , e.g. on failed authentication
    else {
      // As call allready throws an exception, we can return an empty array to
      // break no code.
      return array();
    }
  }

  /**
   * Fetch the public timeline
   *
   * @see http://apiwiki.tumblr.com/Tumblr-REST-API-Method%3A-statuses-public_timeline
   */
  public function public_timeline() {
    return $this->get_statuses('statuses/public_timeline');
  }

  /**
   * Fetch the authenticated user's friends timeline
   *
   * @see http://apiwiki.tumblr.com/Tumblr-REST-API-Method%3A-statuses-friends_timeline
   */
  public function friends_timeline($params = array()) {
    return $this->get_statuses('statuses/friends_timeline', $params, TRUE);
  }

  /**
   * Fetch a user's timeline
   *
   * @see http://apiwiki.tumblr.com/Tumblr-REST-API-Method%3A-statuses-user_timeline
   */
  public function user_timeline($id, $params = array(), $use_auth = FALSE) {
    if (is_numeric($id)) {
      $params['user_id'] = $id;
    }
    else {
      $params['screen_name'] = $id;
    }

    return $this->get_statuses('statuses/user_timeline', $params, $use_auth);
  }

  /**
   *
   * @see http://apiwiki.tumblr.com/Tumblr-REST-API-Method%3A-statuses-mentions
   */
  public function mentions($params = array()) {
    return $this->get_statuses('statuses/mentions', $params, TRUE);
  }

  /**
   *
   * @see http://apiwiki.tumblr.com/Tumblr-REST-API-Method%3A-statuses%C2%A0update
   */
  public function status_update($status, $params = array()) {
    $params['status'] = $status;
    if ($this->source) {
      $params['source'] = $this->source;
    }
    $values = $this->call('statuses/update', $params, 'POST', TRUE);

    return new TumblrStatus($values);
  }

  /**
   *
   * @see http://apiwiki.tumblr.com/Tumblr-REST-API-Method%3A-users%C2%A0show
   */
  public function user_info($use_auth = TRUE) {
    $params = array();

    $call = $this->call('user/info', $params, 'GET', $use_auth);
    $response = $call['response'];
    return new TumblrUser($response['user']);
  }

  /**
   * Retrieve Published Posts.
   *
   * @param string $blogName
   *   The standard or custom blog hostname.
   *
   * @param string $type
   *   The type of post to return, if empty will bring all, or specify one of
   *   the following:
   *    - text
   *    - quote
   *    - link
   *    - answer
   *    - video
   *    - audio
   *    - photo
   *    - chat
   *
   * @param array $params
   *   Required parameters to do a request.
   *   Parameters:
   *    - api_key: Application key.
   *
   * @return array
   *   Retrieve Published Posts Objects.
   *
   * @see http://www.tumblr.com/docs/en/api/v2#posts
   */
  public function getBlogPosts($blogName, $type = '', $params = array()) {
    $posts = array();

    // Validate if blog name has the .tumblr.com suffix.
    if (!stripos('.', $blogName)) {
      $blogName .= '.tumblr.com';
    }

    // Build path.
    $path = 'blog/' . $blogName . '/posts';

    $allowed_types = tumblr_get_posts_types();

    // Bring an specific type if set.
    if (!empty($type) && in_array($type, $allowed_types)) {
        $path .= "/$type";
    }

    // Make the call to Tumblr.
    $call = $this->call($path, $params, 'GET', TRUE);

    // If it returns posts, create an TumblrPost object for each one.
    if (isset($call['response']['posts'])) {
      foreach ($call['response']['posts'] as $key => $post) {
        $posts[] = new TumblrPost($post);
      }
    }

    return $posts;
  }

   /**
   * Retrieve a Blog Avatar.
   *
   * @param string $blogName
   *   The standard or custom blog hostname.
   *
   * @param string $size
   *   The size of the avatar (square, one value for both length and width).
   *   Possible values: 16, 24, 30, 40, 48, 64, 96, 128, 512.
   *
   * @param array $params
   *   Required parameters to do a request.
   *   Parameters:
   *    - api_key: Application key.
   *
   * @return array
   *   Retrieve a Blog Avatar.
   *
   * @see http://www.tumblr.com/docs/en/api/v2#blog-avatar
   */
  public function getBlogAvatar($blogName, $size = NULL, $params = array()) {
    $response = array();

    // Validate if blog name has the .tumblr.com suffix.
    if (!stripos('.', $blogName)) {
      $blogName .= '.tumblr.com';
    }

    // Build path.
    $path = 'blog/' . $blogName . '/avatar';

    if ($size) {
        $path .= "/$size";
    }

    if (isset($params['api_key']) && !empty($params['api_key'])) {
      $url = $this->create_url($path, '');
      return $this->doRequestByCURL($params['api_key'], $url);
    }

    return $response;
  }

  /**
   * Retrieve Blog Info.
   *
   * @param string $blogName
   *   The standard or custom blog hostname.
   *
   * @param  boolean $use_auth
   *   Whether connection is using OAuth.
   *
   * @param array $params
   *   Required parameters to do a request.
   *   Parameters:
   *    - api_key: Application key.
   *
   * @return object
   *   Blog Info TumblrBlog.
   *
   * @see http://www.tumblr.com/docs/en/api/v2#blog-info
   */
  public function getBlogInfo($blog_name, $use_auth = FALSE, $params = array()) {
    $params['api_key'] = $this->consumer->key;
    if (!stripos('.', $blog_name)) {
      $blog_name .= '.tumblr.com';
    }

    $values = $this->call('blog/' . $blog_name . '/info', $params, 'GET', $use_auth);

    return new TumblrBlog($values['response']['blog']);
  }

  /**
   *
   * @see http://apiwiki.tumblr.com/Tumblr-REST-API-Method%3A-account%C2%A0verify_credentials
   */
  public function verify_credentials() {
    $values = $this->call('account/verify_credentials', array(), 'GET', TRUE);
    if (!$values) {
      return FALSE;
    }
    return new TumblrUser($values);
  }

  /**
   * Method for calling any tumblr api resource
   */
  public function call($path, $params = array(), $method = 'GET', $use_auth = FALSE) {
    $url = $this->create_url($path, '');

    try {
      if ($use_auth) {
        $response = $this->auth_request($url, $params, $method);
      }
      else {
        $response = $this->request($url, $params, $method);
      }
    }
    catch (TumblrException $e) {
      // watchdog('tumblr', '!message', array('!message' => $e->__toString()), WATCHDOG_ERROR);
      watchdog_exception('tumblr', $e, '%type: !message while accessing !endpoint in %function (line %line of %file).', array('!endpoint' => $url));
      return FALSE;
    }

    if (!$response) {
      return FALSE;
    }

    return $this->parse_response($response);
  }

  /**
   * Perform an authentication required request.
   */
  protected function auth_request($path, $params = array(), $method = 'GET') {
    if (empty($this->api_key)) {
      print 'Apikey is missing';
      return false;
    }

    return $this->request($path, $params, $method, TRUE);
  }

  /**
   * Perform a request
   */
  protected function request($url, $params = array(), $method = 'GET', $add_api_key = TRUE) {
    $data = '';
    $headers = array();

    $headers['Authorization'] = 'Oauth';
    $headers['Content-type'] = 'application/x-www-form-urlencoded';

    if ($add_api_key) {
      $params = array_merge(
        array('api_key' => $this->consumer->key), $params ? : array()
      );
    }

    if (count($params) > 0) {
      if ($method == 'GET') {
        $url .= '?' . http_build_query($params, '', '&');
      }
      else {
        $data = http_build_query($params, '', '&');
      }
    }

    $response = $this->doRequest($url, $headers, $method, $data);

    if (!isset($response->error)) {
      return $response->data;
    }
    else {
      $error = $response->error;
      $data = $this->parse_response($response->data);
      if (isset($data['error'])) {
        $error = $data['error'];
      }
      throw new TumblrException($error);
    }
  }

  /**
   * Actually performs a request.
   *
   * This method can be easily overriden through inheritance.
   *
   * @param string $url
   *   The url of the endpoint.
   * @param array $headers
   *   Array of headers.
   * @param string $method
   *   The HTTP method to use (normally POST or GET).
   * @param array $data
   *   An array of parameters
   * @return
   *   stdClass response object.
   */
  protected function doRequest($url, $headers, $method, $data) {
    return drupal_http_request($url, array('headers' => $headers, 'method' => $method, 'data' => $data));
  }

  protected function parse_response($response, $format = NULL) {
    if (empty($format)) {
      $format = $this->format;
    }

    switch ($format) {
      case 'json':
        // http://drupal.org/node/985544 - json_decode large integer issue
        $length = strlen(PHP_INT_MAX);
        $response = preg_replace('/"(id|in_reply_to_status_id)":(\d{' . $length . ',})/', '"\1":"\2"', $response);
        return json_decode($response, TRUE);
    }
  }

  protected function create_url($path, $format = NULL, $host = 'api') {
    if (is_null($format)) {
      $format = $this->format;
    }
    $conf = TumblrConf::instance();
    $url = 'http://' . $conf->get($host) . '/' . $path;
    if (!empty($format)) {
      $url .= '.' . $this->format;
    }
    return $url;
  }

  /**
   * Does a request using CURL. Used for 301 responses.
   *
   * @param string $api_key
   *   Application key.
   *
   * @param string $url
   *   URL to do a request.
   *
   * @return array
   *   CURL response.
   */
  protected function doRequestByCURL($api_key, $url) {
    $result = array();

    $url .= "?app_key=" . $api_key;

    $curl_handle = curl_init();
    curl_setopt($curl_handle, CURLOPT_URL, $url);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($curl_handle);
    curl_close($curl_handle);

    if ($response) {
      $result = drupal_json_decode($response);
      return $result['response'];
    }

    return $result;
  }

}

/**
 * A class to provide OAuth enabled access to the tumblr API
 */
class TumblrOAuth extends Tumblr {

  protected $signature_method;
  protected $consumer;
  protected $token;
  protected $consumer_key;
  protected $consumer_secret;

  public function __construct($consumer_key, $consumer_secret, $oauth_token = NULL, $oauth_token_secret = NULL) {
    $this->api_key = $consumer_key;
    $this->consumer_key = $consumer_key;
    $this->consumer_secret = $consumer_secret;
    $this->signature_method = new OAuthSignatureMethod_HMAC_SHA1();
    $this->consumer = new OAuthConsumer($consumer_key, $consumer_secret);
    if (!empty($oauth_token) && !empty($oauth_token_secret)) {
      $this->token = new OAuthConsumer($oauth_token, $oauth_token_secret);
    }
  }

  public function get_request_token() {
    $url = $this->create_url('oauth/request_token', '', 'host');
    try {
      $response = $this->auth_request($url);
    }
    catch (TumblrException $e) {
      watchdog_exception('tumblr', $e);
    }

    parse_str($response, $token);
    $this->token = new OAuthConsumer($token['oauth_token'], $token['oauth_token_secret']);
    return $token;
  }

  public function get_authorize_url($token) {
    $url = $this->create_url('oauth/authorize', '', 'host');
    $url.= '?oauth_token=' . $token['oauth_token'];

    return $url;
  }

  public function get_authenticate_url($token) {
    $url = $this->create_url('oauth/authenticate', '', 'host');
    $url.= '?oauth_token=' . $token['oauth_token'];

    return $url;
  }

  public function get_access_token($oauth_verifier = NULL) {
    $url = $this->create_url('oauth/access_token', '', 'host');

    // Adding parameter oauth_verifier to auth_request
    $parameters = array();
    if (!empty($oauth_verifier)) {
      $parameters['oauth_verifier'] = $oauth_verifier;
    }

    try {
      $response = $this->auth_request($url, $parameters);
    }
    catch (TumblrException $e) {
      watchdog_exception('Tumblr', $e);
    }
    parse_str($response, $token);
    $this->token = new OAuthConsumer($token['oauth_token'], $token['oauth_token_secret']);
    return $token;
  }

  public function auth_request($url, $params = array(), $method = 'GET', $add_api_key = FALSE) {
    $request = OAuthRequest::from_consumer_and_token($this->consumer, $this->token, $method, $url, $params);
    $request->sign_request($this->signature_method, $this->consumer, $this->token);
    switch ($method) {
      case 'GET':
        return $this->request($request->to_url());
      case 'POST':
        return $this->request($request->get_normalized_http_url(), $request->get_parameters(), 'POST');
    }
  }

}

/**
 * Class for containing an individual tumblr status.
 */
class TumblrPost {

  /**
   * @var created_at
   */
  public $id;
  public $blog_name;
  public $post_url;
  public $slug;
  public $type;
  public $date;
  public $timestamp;
  public $state;
  public $format;
  public $reblog_key;
  public $tags;
  public $short_url;
  public $highlighted;
  public $caption;
  public $mobile;
  public $feed_item;
  public $from_feed_id;
  public $title;
  public $body;
  public $photos;
  public $player;
  public $source;
  public $source_url;
  public $source_title;
  public $text;
  public $url;
  public $followed;
  public $liked;
  public $note_count;
  public $image_permalink;
  public $can_reply;

  /**
   * Constructor for TumblrPost.
   */
  public function __construct($values = array()) {
    $values = array_filter($values);

    foreach ($values as $key => $value) {
      if (property_exists($this, $key)) {
        $this->{$key} = $values[$key];
      }
    }
  }
}

class TumblrBlog {

  /**
   * @var id
   */
  public $title;
  public $name;
  public $posts;
  public $url;
  public $updated;
  public $description;
  public $ask;
  public $ask_anon;
  public $is_nsfw;
  public $followed;
  public $can_send_fan_mail;
  public $share_likes;
  public $likes;
  public $twitter_enabled;
  public $twitter_send;
  public $facebook_opengraph_enabled;
  public $tweet;
  public $avatar_url;
  public $facebook;
  protected $oauth_token;
  protected $oauth_token_secret;

  public function __construct($values = array()) {
    $values = array_filter($values);

    foreach ($values as $key => $value) {
      if (property_exists($this, $key)) {
        $this->{$key} = $values[$key];
      }
    }
  }

  public function get_auth() {
    return array('oauth_token' => $this->oauth_token, 'oauth_token_secret' => $this->oauth_token_secret);
  }

  public function set_auth($values) {
    $this->oauth_token = isset($values['oauth_token']) ? $values['oauth_token'] : NULL;
    $this->oauth_token_secret = isset($values['oauth_token_secret']) ? $values['oauth_token_secret'] : NULL;
  }

}

class TumblrUser {

  /**
   * @var id
   */
  public $id;
  public $name;
  public $likes;
  public $following;
  public $default_post_format;
  public $blogs;
  protected $oauth_token;
  protected $oauth_token_secret;

  public function __construct($values = array()) {

    $values = array_filter($values);

    foreach ($values as $key => $value) {
      if (property_exists($this, $key)) {
        $this->{$key} = $values[$key];
      }
    }
  }

  public function get_auth() {
    return array('oauth_token' => $this->oauth_token, 'oauth_token_secret' => $this->oauth_token_secret);
  }

  public function set_auth($values) {
    $this->oauth_token = isset($values['oauth_token']) ? $values['oauth_token'] : NULL;
    $this->oauth_token_secret = isset($values['oauth_token_secret']) ? $values['oauth_token_secret'] : NULL;
  }

}
