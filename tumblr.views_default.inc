<?php

/**
 * Implements hook_views_default_views().
 */
function tumblr_views_default_views() {

  // Main information about Views.
  $view = new view();
  $view->name = 'tumblr';
  $view->description = 'Displays Tumblr posts for users who have associated Tumblr accounts.';
  $view->tag = 'default';
  $view->base_table = 'tumblr_post';
  $view->human_name = 'Tumblr';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'tumblr';
  /* Field: Tumblr: UID */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'tumblr_post';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['label'] = '';
  $handler->display->display_options['fields']['uid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['uid']['element_label_colon'] = FALSE;
  /* Field: Tumblr: Post ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'tumblr_post';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['label'] = '';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  /* Field: Tumblr: Player */
  $handler->display->display_options['fields']['player']['id'] = 'player';
  $handler->display->display_options['fields']['player']['table'] = 'tumblr_post';
  $handler->display->display_options['fields']['player']['field'] = 'player';
  $handler->display->display_options['fields']['player']['label'] = '';
  $handler->display->display_options['fields']['player']['element_label_colon'] = FALSE;
  /* Field: Tumblr: Photo */
  $handler->display->display_options['fields']['photos']['id'] = 'photos';
  $handler->display->display_options['fields']['photos']['table'] = 'tumblr_post';
  $handler->display->display_options['fields']['photos']['field'] = 'photos';
  $handler->display->display_options['fields']['photos']['label'] = '';
  $handler->display->display_options['fields']['photos']['element_label_colon'] = FALSE;
  /* Field: Tumblr: Created */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'tumblr_post';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['label'] = '';
  $handler->display->display_options['fields']['timestamp']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['timestamp']['date_format'] = 'custom';
  $handler->display->display_options['fields']['timestamp']['custom_date_format'] = 'F d, Y ';
  /* Field: Tumblr: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'tumblr_post';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['link_urls'] = 1;
  $handler->display->display_options['fields']['body']['link_usernames'] = 1;
  $handler->display->display_options['fields']['body']['link_hashtags'] = 0;
  /* Field: Tumblr: Source */
  $handler->display->display_options['fields']['source']['id'] = 'source';
  $handler->display->display_options['fields']['source']['table'] = 'tumblr_post';
  $handler->display->display_options['fields']['source']['field'] = 'source';
  $handler->display->display_options['fields']['source']['label'] = '';
  $handler->display->display_options['fields']['source']['element_label_colon'] = FALSE;
  /* Sort criterion: Tumblr: Created date */
  $handler->display->display_options['sorts']['date']['id'] = 'date';
  $handler->display->display_options['sorts']['date']['table'] = 'tumblr_post';
  $handler->display->display_options['sorts']['date']['field'] = 'date';
  $handler->display->display_options['sorts']['date']['order'] = 'DESC';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'user/%/tumblr';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Tumblr';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');

  return array('tumblr' => $view);
}
