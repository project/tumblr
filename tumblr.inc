<?php

/**
 * tumblr API functions
 */

/**
 * Connect to the API using the 'proper' version (Oauth vs. standard)
 */
function tumblr_connect($account) {
  $auth = $account->get_auth();
  if (_tumblr_use_oauth() && $auth['oauth_token'] && $auth['oauth_token_secret']) {
    module_load_include('lib.php', 'oauth_common');
    return new tumblrOAuth(variable_get('tumblr_consumer_key', ''), variable_get('tumblr_consumer_secret', ''),
        $auth['oauth_token'], $auth['oauth_token_secret']);
  }
  elseif ($auth['password']) {
    return new tumblr($account->name, $auth['password']);
  }
  else {
    return new tumblr;
  }
}

/**
 * Saves a tumblr user object to {tumblr_account}.
 */
function tumblr_account_save($tumblr, $save_auth = TRUE, $account = NULL) {
  $values = (array) $tumblr;

  // bool => int for DB storage
  foreach (array('likes', 'following') as $k) {
    if (isset($values[$k])) {
      $values[$k] = (int) $values[$k];
    }
  }
  foreach (array('blogs') as $k) {
    if (isset($values[$k])) {
      $values[$k] = serialize($values[$k]);
    }
  }

  if ($save_auth && is_object($tumblr)) {
    $values += $tumblr->get_auth();
  }

  if (empty($account)) {
    global $user;
    $account = $user;
  }

  $values['uid'] = $account->uid;

  $schema = drupal_get_schema('tumblr_account');
  foreach ($values as $k => $v) {
    if (!isset($schema['fields'][$k])) {
      unset($values[$k]);
    }
  }

  db_merge('tumblr_account')
    ->key(array('uid' => $values['uid'], 'name' => $values['name']))
    ->fields($values)
    ->execute();
}

/**
 * Saves a tumblr blog object to {tumblr_blog}
 */
function tumblr_blog_save($blog = array(), $account = NULL) {
  $values = (array) $blog;

  // bool => int for DB storage
  foreach (array('likes', 'posts') as $k) {
    if (isset($values[$k])) {
      $values[$k] = (int) $values[$k];
    }
    else {
      $values[$k] = 0;
    }
  }

  // Convert to bool values.
  foreach (array('ask', 'ask_anon', 'is_nsfw', 'share_likes') as $k) {
    if (isset($values[$k])) {
      $values[$k] = (bool) ($values[$k]);
    }
  }

  if (empty($account)) {
    global $user;
    $account = $user;
  }

  $values['uid'] = $account->uid;

  $schema = drupal_get_schema('tumblr_blog');
  foreach ($values as $k => $v) {
    if (!isset($schema['fields'][$k])) {
      unset($values[$k]);
    }
  }

  db_merge('tumblr_blog')
    ->key(array('uid' => $values['uid'], 'name' => $values['name']))
    ->fields($values)
    ->execute();
}

/**
 * Save a tumblr blog object to {tumblr_post}.
 *
 * @param Object $post
 *   User post.
 *
 * @param array $account
 *   User's tumbler main account information.
 */
function tumblr_posts_save($post, $account = NULL) {

  $values = (array) $post;

  // Cast values to int for DB storage.
  foreach (array('timestamp', 'bookmarklet', 'mobile') as $k) {
    if (isset($values[$k])) {
      $values[$k] = (int) $values[$k];
    }
    else {
      $values[$k] = 0;
    }
  }

  // Serialize photos values.
  foreach (array('photos', 'player', 'tags') as $k) {
    if (isset($values[$k])) {
      $values[$k] = serialize($values[$k]);
    }
  }

  // If account is not provided, use current user.
  if (empty($account)) {
    global $user;
    $account = $user;
  }

  // Clean caption and description values.
  foreach (array('caption', 'description') as $k) {
    if (isset($values[$k])) {
      $values[$k] = tumblr_remove_emoji(strip_tags(filter_xss($values[$k])));
    }
  }

  // User id.
  $values['uid'] = $account->uid;

  // Retrieve fields from {tumblr_post} table and check them agains values, in
  // order to insert the correct ones.
  $schema = drupal_get_schema('tumblr_post');
  watchdog(__FUNCTION__, '$schema field keys <pre>' . print_r(array_keys($schema['fields']), 1) . '</pre>');
  foreach ($values as $k => $v) {
    if (!isset($schema['fields'][$k])) {
      unset($values[$k]);
    }
  }
  watchdog(__FUNCTION__, '$values keys: <pre>' . print_r(array_keys($values), 1) . '</pre>');
  watchdog(__FUNCTION__, '$values: <pre>' . print_r($values, 1) . '</pre>');

  // Insert or update values on {tumblr_post}.
  db_merge('tumblr_post')
    ->key(array('id' => $values['id']))
    ->fields($values)
    ->execute();
}

/**
 * Load a tumblr account from {tumblr_account}.
 *
 * @param $id
 *   tumblr UID
 *
 * @return
 *   tumblrUser object
 *
 */
function tumblr_account_load($uid) {
  module_load_include('php', 'tumblr', 'tumblr.lib');
  if ($values = db_query("SELECT * FROM {tumblr_account} WHERE uid = :uid", array(':uid' => $uid))->fetchAssoc()) {

    // $values['id'] = $values['uid'];
    $tumblr_account = new TumblrUser($values);
    $tumblr_account->set_auth($values);
    $tumblr_account->uid = $values['uid'];
    $tumblr_account->tumblr_blogs = tumblr_blog_load($values['uid']);
    return $tumblr_account;
  }
}

/**
 * Load a tumblr account from {tumblr_account}.
 *
 * @param $id
 *   tumblr UID
 *
 * @return
 *   tumblrUser object
 *
 */
function tumblr_blog_load($uid) {
  $blogs = array();
  module_load_include('php', 'tumblr', 'tumblr.lib');
  if ($result = db_query("SELECT * FROM {tumblr_blog} WHERE uid = :uid", array(':uid' => $uid))) {
    while ($blog = $result->fetchAssoc()) {
      $tumblr_blog = new TumblrBlog($blog);
      $tumblr_blog->id = $blog['id'];
      $tumblr_blog->uid = $blog['uid'];
      $tumblr_blog->import = $blog['import'];
      $tumblr_blog->is_global = $blog['is_global'];
      $blogs[] = $tumblr_blog;
    }

    return $blogs;
  }
}

/**
 * Load Tumblr Posts of a specific account from {tumblr_post}.
 *
 * @param int $uid
 *   User id.
 *
 * @param string $type
 *   Optional parameter for Tumblr Post type, could be one of the following:
 *    - text
 *    - quote
 *    - link
 *    - answer
 *    - video
 *    - audio
 *    - photo
 *    - chat.
 *
 * @param string $pid
 *   Optional Post identificator.
 *
 * @return array
 *   Tumblr Posts of a specific account.
 */
function tumblr_post_load($uid = NULL, $type = '', $pid = '') {
  $posts = array();
  $values = array();
  $query = '';

  // Get posts.
  if (!empty($type) || !empty($pid)) {

    if (!empty($type) && empty($pid)) {
      $query = "SELECT * FROM {tumblr_post} WHERE type = :type";
      $values = array(
        ':type' => $type,
      );
    }
    elseif (empty($type) && !empty($pid)) {
      $query = "SELECT * FROM {tumblr_post} WHERE id = :id";
      $values = array(
        ':id' => $pid,
      );
    }
    elseif (!empty($type) && !empty($pid)) {
      $query = "SELECT * FROM {tumblr_post} WHERE type = :type AND id = :id";
      $values = array(
        ':id' => $pid,
        ':type' => $type,
      );
    }
  }
  else {
    $query = "SELECT * FROM {tumblr_post}";
    $values = array();
  }

  // Execute query.
  $result = db_query($query, $values);

  // If there are results.
  if ($result) {

    // Use Tumblr library.
    module_load_include('php', 'tumblr', 'tumblr.lib');

    while ($post = $result->fetchAssoc()) {
      $tumblr_post = new TumblrPost($post);
      $posts[] = $tumblr_post;
    }

    return $posts;
  }

  return $posts;
}

function tumblr_get_blog_uid($id) {
  $query = db_select('tumblr_blog', 'tb')
    ->fields('tb', array('uid'))
    ->condition('tb.id', $id);
  return $query->execute()->fetchCol();
}

function tumblr_get_blog_name($id) {
  $query = db_select('tumblr_blog', 'tb')
    ->fields('tb', array('name'))
    ->condition('tb.id', $id);
  return $query->execute()->fetchCol();
}

/**
 * Saves a tumblrStatus object to {tumblr}
 */
function tumblr_status_save($status) {
  $status = array(
    'tumblr_id' => $status->id,
    'name' => $status->user->name,
    'created_time' => strtotime($status->created_at),
    'text' => $status->text,
    'source' => $status->source,
    'in_reply_to_status_id' => ($status->in_reply_to_status_id > 0) ? (string) $status->in_reply_to_status_id : NULL,
    'in_reply_to_user_id' => (int) $status->in_reply_to_user_id,
    'in_reply_to_name' => $status->in_reply_to_name,
    'truncated' => (int) $status->truncated,
  );
  db_merge('tumblr')
    ->key(array('tumblr_id' => $status['tumblr_id']))
    ->fields($status)
    ->execute();
}

/**
 * Post a message to tumblr
 */
function tumblr_set_status($tumblr_account, $status) {
  $tumblr = tumblr_connect($tumblr_account);
  $tumblr->status_update($status);
}

/**
 * Fetches a user's timeline
 */
function tumblr_fetch_user_posts($id) {
  $account = tumblr_account_load($id);
  $tumblr = tumblr_connect($account);
  if (isset($tumblr->api_key)) {
    $posts = $tumblr->getBlogPosts($account->name, '', array('api_key' => $tumblr->api_key));

    if (is_array($posts) && !empty($posts)) {
      // Save posts.
      foreach ($posts as $key => $post) {
        tumblr_posts_save($post, $account);
      }
    }

    db_update('tumblr_blog')
      ->fields(array(
        'last_refresh' => REQUEST_TIME,
      ))
      ->condition('uid', $account->id)
      ->execute();
  }
}

/**
 * Delete a tumblr account and its statuses.
 *
 * @param $uid
 *   An integer with the tumblr UID.
 *
 * @param $name
 *   Optional string with the user name.
 */
function tumblr_account_delete($id) {
  $name = tumblr_get_blog_name($id);

  // Delete from {tumblr_blog}.
  $query = db_delete('tumblr_blog');
  $query->conditions('id', $id);
  $query->execute();

  // Delete from {tumblr_account}.
  $query = db_delete('tumblr_account');
  $query->conditions('name', $name);
  $query->execute();

  // Delete from {tumblr}.
  $query = db_delete('tumblr_post');
  $query->conditions('name', $name);
  $query->execute();

  // Delete from {tumblr_account}.
  $query = db_delete('authmap');
  $query->conditions('authname', $id);
  $query->conditions('module', 'tumblr');
  $query->execute();
}
