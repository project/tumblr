<?php

/**
 * @file
 * Provide views data and handlers for tumblr.module
 */

/**
 * @defgroup views_tumblr_module tumblr.module handlers
 *
 * Includes the ability to create views of just the tumblr table.
 * @{
 */

/**
 * Implements hook_views_data().
 */
function tumblr_views_data() {
  // Basic table information.

  $data['tumblr_post']['table']['group'] = t('Tumblr');

  // Advertise this table as a possible base table.
  $data['tumblr_post']['table']['base'] = array(
    'field' => 'tumblr_id',
    'title' => t('Tumblr Posts'),
    'help' => t('Tumblr posts.'),
    'weight' => 10,
  );

  // Post ID.
  $data['tumblr_post']['id'] = array(
    'title' => t('Post ID'),
    'help' => t('The ID of the Tumblr post.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Tumblr reply-to-status id.
  $data['tumblr_post']['uid'] = array(
    'title' => t('UID'),
    'help' => t('The UID of the author'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Tumblr Blog Name.
  $data['tumblr_post']['blog_name'] = array(
    'title' => t('Blog Name'),
    'help' => t('The name of the blog.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Tumblr Post URL.
  $data['tumblr_post']['post_url'] = array(
    'title' => t('Post URL'),
    'help' => t('The URL of the post.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Tumblr post slug.
  $data['tumblr_post']['slug'] = array(
    'title' => t('Slug'),
    'help' => t('The post\'s slug.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Tumblr type.
  $data['tumblr_post']['type'] = array(
    'title' => t('Post Type'),
    'help' => t('The type of the Tumblr Post.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // @TODO: Decide if we want to allow argument handling for this field
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Tumblr message date.
  $data['tumblr_post']['date'] = array(
    'title' => t('Created date'),
    'help' => t('The GMT date and time when the Tumblr post message was posted, as a string.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Tumblr message timestamp.
  $data['tumblr_post']['timestamp'] = array(
    'title' => t('Created'),
    'help' => t('The date as timestamp when the Tumblr message was posted.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Tumblr Status.
  $data['tumblr_post']['state'] = array(
    'title' => t('Post State'),
    'help' => t('The state of the Tumblr Post.'),
        'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // @TODO: Decide if we want to allow argument handling for this field
    // 'argument' => array(
    //  'handler' => 'views_handler_argument_string',
    // ),
  );

  // Tumblr Format.
  $data['tumblr_post']['format'] = array(
    'title' => t('Post Format'),
    'help' => t('The format of the Tumblr Post.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // @TODO: Decide if we want to allow argument handling for this field
    // 'argument' => array(
    //  'handler' => 'views_handler_argument_string',
    // ),
  );

  // Tumblr tags.
  $data['tumblr_post']['tags'] = array(
    'title' => t('Post Tags'),
    'help' => t('The tags of the Tumblr Post.'),
        'field' => array(
      'handler' => 'views_handler_field_serialized',
      'click sortable' => TRUE,
    ),
    // 'filter' => array(
    //   'handler' => 'views_handler_filter_string',
    // ),
    // 'sort' => array(
    //   'handler' => 'views_handler_sort',
    // ),
    // @TODO: Decide if we want to allow argument handling for this field
    // 'argument' => array(
    //  'handler' => 'views_handler_argument_string',
    // ),
  );

  // Tumblr title.
  $data['tumblr_post']['title'] = array(
    'title' => t('Tumblr post title'),
    'help' => t('The title of the Tumblr message.'),
    'field' => array(
      'handler' => 'tumblr_views_handler_field_xss',
      'click sortable' => TRUE,
    ),
    // 'filter' => array(
    //   'handler' => 'views_handler_filter_string',
    // ),
  );

  // Tumblr body.
  $data['tumblr_post']['body'] = array(
    'title' => t('Body'),
    'help' => t('The body of the Tumblr message.'),
    'field' => array(
      'handler' => 'tumblr_views_handler_field_xss',
      'click sortable' => TRUE,
    ),
    // 'filter' => array(
    //   'handler' => 'views_handler_filter_string',
    // ),
  );

  // Tumblr text.
  $data['tumblr_post']['text'] = array(
    'title' => t('Text'),
    'help' => t('The text of the Tumblr message.'),
    'field' => array(
      'handler' => 'tumblr_views_handler_field_xss',
      'click sortable' => TRUE,
    ),
    // 'filter' => array(
    //   'handler' => 'views_handler_filter_string',
    // ),
  );

  // Tumblr tags.
  $data['tumblr_post']['player'] = array(
    'title' => t('Player'),
    'help' => t('Tumblr Post player.'),
    'field' => array(
      'handler' => 'tumblr_handler_field_post_player',
      'click sortable' => TRUE,
    ),
    // 'filter' => array(
    //   'handler' => 'views_handler_filter_string',
    // ),
    // 'sort' => array(
    //   'handler' => 'views_handler_sort',
    // ),
    // @TODO: Decide if we want to allow argument handling for this field
    // 'argument' => array(
    //  'handler' => 'views_handler_argument_string',
    // ),
  );

  // Tumblr tags.
  $data['tumblr_post']['photos'] = array(
    'title' => t('Photo'),
    'help' => t('Tumblr Post photo.'),
    'field' => array(
      'handler' => 'tumblr_handler_field_post_photos',
      'click sortable' => TRUE,
    ),
    // 'filter' => array(
    //   'handler' => 'views_handler_filter_string',
    // ),
    // 'sort' => array(
    //   'handler' => 'views_handler_sort',
    // ),
    // @TODO: Decide if we want to allow argument handling for this field
    // 'argument' => array(
    //  'handler' => 'views_handler_argument_string',
    // ),
  );

  // Tumblr source.
  $data['tumblr_post']['source'] = array(
    'title' => t('Source'),
    'help' => t('The name of the application that posted the Tumblr message.'),
    'field' => array(
      'handler' => 'tumblr_views_handler_field_xss',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['tumblr_account']['table']['group'] = t('Tumblr');
  $data['tumblr_account']['table']['join'] = array(
    'tumblr' => array(
      'left_field' => 'screen_name',
      'field' => 'screen_name',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    //      'left_table' => 'tumblr_user',
    ),
  );

  // Tumblr screen name.
  $data['tumblr_account']['screen_name'] = array(
    'title' => t('Login name'),
    'help' => t('The login account of the Tumblr user.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  // Tumblr account full name.
  $data['tumblr_account']['name'] = array(
    'title' => t('Full name'),
    'help' => t('The full name Tumblr account user.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Tumblr account description.
  $data['tumblr_account']['description'] = array(
    'title' => t('Description'),
    'help' => t('The description of the Tumblr account.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_field_xss',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Tumblr account location.
  $data['tumblr_account']['location'] = array(
    'title' => t('Location'),
    'help' => t('The location of the Tumblr account.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_field_xss',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Tumblr account followers count.
  $data['tumblr_account']['followers_count'] = array(
    'title' => t('Followers'),
    'help' => t('The number of users following this Tumblr account.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Tumblr account profile image.
  $data['tumblr_account']['profile_image_url'] = array(
    'title' => t('Profile image'),
    'help' => t('The image used by the Tumblr account.'),
    'field' => array(
      'handler' => 'tumblr_views_handler_field_profile_image',
      'click sortable' => TRUE,
    ),
  );

  // Tumblr account url.
  $data['tumblr_account']['url'] = array(
    'title' => t('URL'),
    'help' => t('The URL given by the Tumblr account user.'),
    'field' => array(
      'handler' => 'views_handler_field_url',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Tumblr account protected.
  $data['tumblr_account']['protected'] = array(
    'title' => t('Protected status'),
    'help' => t('Whether posts from this Tumblr account should be visible to the general public.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Tumblr message timestamp.
  $data['tumblr_account']['last_refresh'] = array(
    'title' => t('Last refresh'),
    'help' => t('The time the Tumblr account statuses were retrieved.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Tumblr account description.
  $data['tumblr_account']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('The UID of the Tumblr account.'),
  );
  // Tumblr account protected.
  $data['tumblr_account']['import'] = array(
    'title' => t('Import status'),
    'help' => t('Whether posts from this Tumblr account should be imported automatically.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function tumblr_views_data_alter(&$data) {
  $data['users']['table']['join']['tumblr'] = array(
    'left_table' => 'tumblr_account',
    'left_field' => 'uid',
    'field' => 'uid',
  );
  $data['users']['table']['join']['tumblr_account'] = array(
    //    'left_table' => 'tumblr_user',
    'left_field' => 'uid',
    'field' => 'uid',
  );
}

/**
 * Implements hook_views_plugins().
 */
function tumblr_views_plugins() {
  return array(
    'row' => array(
      'tumblr' => array(
        'title' => t('Tumblr Post'),
        'help' => t('Display the content of a Tumblr post.'),
        'handler' => 'tumblr_plugin_row_post_view',
        'base' => array('tumblr_post'), // only works with 'tumblr_post' as base.
        'uses options' => FALSE,
        'type' => 'normal',
        'help topic' => 'style-node',
      ),
    ),
  );
}

/**
 * @}
 */
