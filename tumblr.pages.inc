<?php

/**
 * Form builder; Tumblr settings form.
 */
function tumblr_admin_form($form, &$form_state) {
  $form['images'] = array(
    '#type' => 'fieldset',
    '#title' => t('Images Settings'),
    '#description' => t('Use an image style for tumblr photos.'),
  );

  $form['images']['tumblr_image_style'] = array(
    '#type' => 'textfield',
    '#title' => t('Image style name'),
    '#default_value' => variable_get('tumblr_image_style', NULL),
    '#description' => t('Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
  );

  $form['tumblr_import'] = array(
    '#type' => 'checkbox',
    '#title' => t('Import and display the Tumblr statuses of site users who have entered their Tumblr account information.'),
    '#default_value' => variable_get('tumblr_import', 1),
  );
  $form['tumblr_expire'] = array(
    '#type' => 'select',
    '#title' => t('Delete old statuses'),
    '#default_value' => variable_get('tumblr_expire', 0),
    '#options' => array(0 => t('Never')) + drupal_map_assoc(array(604800, 2592000, 7776000, 31536000), 'format_interval'),
    '#states' => array(
      'visible' => array(
        ':input[name=tumblr_import]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['oauth'] = array(
    '#type' => 'fieldset',
    '#title' => t('OAuth Settings'),
    '#access' => module_exists('oauth_common'),
    '#description' => t('To enable OAuth based access for tumblr, you must <a href="@url">register your application</a> with Tumblr and add the provided keys here.', array('@url' => 'https://dev.tumblr.com/apps/new')),
  );
  $form['oauth']['callback_url'] = array(
    '#type' => 'item',
    '#title' => t('Callback URL'),
    '#markup' => url('tumblr/oauth', array('absolute' => TRUE)),
  );
  $form['oauth']['tumblr_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer key'),
    '#default_value' => variable_get('tumblr_consumer_key', NULL),
  );
  $form['oauth']['tumblr_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer secret'),
    '#default_value' => variable_get('tumblr_consumer_secret', NULL),
  );

  return system_settings_form($form);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function tumblr_user_settings($account) {
  module_load_include('inc', 'tumblr');
  $output = array();
  if (!empty($account->tumblr_accounts)) {
    $output['list_form'] = drupal_get_form('tumblr_account_list_form', $account->tumblr_accounts);
  }
  $output['form'] = drupal_get_form('tumblr_account_form', $account);

  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function tumblr_account_list_form($form, $form_state, $tumblr_accounts = array()) {
  $form['#tree'] = TRUE;
  $form['accounts'] = array();

  foreach ($tumblr_accounts as $tumblr_account) {
    $form['accounts'][] = _tumblr_account_list_row($tumblr_account);
  }

  if (!empty($tumblr_accounts)) {
    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save changes'),
    );
  }

  return $form;
}

function _tumblr_account_list_row($tumblr_blog) {
  $form['#blog'] = $tumblr_blog;

  $form['id'] = array(
    '#type' => 'value',
    '#value' => $tumblr_blog->id,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $tumblr_blog->uid,
  );

  $form['name'] = array(
    '#type' => 'value',
    '#value' => $tumblr_blog->name,
  );

  $form['image'] = array(
    '#markup' => theme('image', array('path' => $tumblr_blog->avatar_url)),
  );

  $form['visible_name'] = array(
    '#markup' => l($tumblr_blog->title, $tumblr_blog->url),
  );

  $form['description'] = array(
    '#markup' => filter_xss($tumblr_blog->description),
  );

  $form['url'] = array(
    '#markup' => $tumblr_blog->url,
  );

  // Here we use user_access('import own tumblr posts') to check permission
  // instead of user_access('import own tumblr posts', $account->uid)
  // because we allow roles with sufficient permission to overwrite
  // the user's import settings.
  if (variable_get('tumblr_import', TRUE) && user_access('import own tumblr posts')) {
    $form['import'] = array(
      '#type' => 'checkbox',
      '#default_value' => user_access('import own tumblr posts') ? $tumblr_blog->import : '',
    );
  }

  $form['delete'] = array(
    '#type' => 'checkbox',
  );

  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function tumblr_account_list_form_submit($form, &$form_state) {
  $accounts = $form_state['values']['accounts'];
  foreach ($accounts as $account) {
    if (empty($account['delete'])) {
      tumblr_account_save($account);
      drupal_set_message(t('The Tumblr account settings were updated.'));
    }
    else {
      tumblr_account_delete($account['id']);
      drupal_set_message(t('The Tumblr account was deleted.'));
    }
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function tumblr_user_make_global($form, $form_state, $account, $tumblr_uid) {
  module_load_include('inc', 'tumblr');

  $tumblr_account = tumblr_account_load($tumblr_uid);

  $form = array();

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );

  $form['tumblr_uid'] = array(
    '#type' => 'value',
    '#value' => $tumblr_uid,
  );

  if ($tumblr_account->is_global) {
    $text = t('Are you sure you want to remove %name from the global accounts?', array('%name' => $tumblr_account->name));
    $description = t('This means other users will no longer be allowed to post using this account.');
  }
  else {
    $text = t('Are you sure you want to allow other users to access the %name account?', array('%name' => $tumblr_account->name));
    $description = t('This will allow other users to post using this account.');
  }

  return confirm_form($form, $text, 'user/' . $account->uid . '/edit/tumblr', $description);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function tumblr_user_make_global_submit($form, &$form_state) {
  db_update('tumblr_account')
    ->expression('is_global', '(1 - is_global)')
    ->condition('tumblr_uid', $form_state['values']['tumblr_uid'])
    ->execute();

  $form_state['redirect'] = 'user/' . $form_state['values']['uid'] . '/edit/tumblr';
}

/**
 * Form to add a Tumblr account
 *
 * If OAuth is not enabled, a text field lets users to add their
 * Tumblr blog short name. If it is, a submit button redirects to
 * Tumblr.com asking for authorisation.
 */
function tumblr_account_form($form, $form_state, $account = NULL) {

  if (empty($account)) {
    global $user;
    $account = $user;
  }

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );

  if (_tumblr_use_oauth()) {
    $form['#validate'] = array('tumblr_account_oauth_validate');

    $form['blog_name'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Blog name'),
      '#description' => t('The standard or custom blog hostname'),
    );

    $form['import'] = array(
      '#type' => 'checkbox',
      '#title' => t('Import statuses from this blog'),
      '#default_value' => TRUE,
      '#access' => FALSE,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add account'),
    );
  }
  elseif ($account->uid == 1 || user_access('add tumblr accounts')) {
    $form['error_message'] = array(
      '#type' => 'item',
      '#title' => t('Link your Tumblr blog to this account'),
      '#markup' => t('oAuth Settings have not been set up. Please !fix this issue and try again.', array('!fix' => l('fix', 'admin/config/services/tumblr'))),
    );
  }

  return $form;
}

/**
 * Implements hook_FORM_ID_submit()
 *
 * Loads Tumblr account details and adds them to the user account
 */
function tumblr_account_form_submit($form, &$form_state) {
  module_load_include('lib.php', 'oauth_common');
  module_load_include('lib.php', 'tumblr');
  module_load_include('inc', 'tumblr');

  $key = variable_get('tumblr_consumer_key', '');
  $secret = variable_get('tumblr_consumer_secret', '');

  $blog_name = $form_state['values']['blog_name'];
  $uid = $form_state['values']['uid'];

  $tumblr = new TumblrOAuth($key, $secret);
  // $tumblr->set_auth($blog_name, $key);
  $tumblr_account = $tumblr->user_info;
  tumblr_account_save($tumblr_account, TRUE, user_load($uid));
}

/**
 * If OAuth is enabled, intercept submission of 'Add Account' form on
 * user/%/edit/tumblr page and redirect to Tumblr for auth.
 */
function tumblr_account_oauth_validate($form, &$form_state) {
  module_load_include('lib.php', 'oauth_common');
  module_load_include('lib.php', 'tumblr');

  $blog_name = $form_state['values']['blog_name'];
  $key = variable_get('tumblr_consumer_key', '');
  $secret = variable_get('tumblr_consumer_secret', '');

  if ($key == '' || $secret == '') {
    form_set_error('', t('Please configure your Tumblr consumer key and secret.'));
  }

  $tumblr = new TumblrOAuth($key, $secret);
  $tumblr->set_auth($blog_name, $key);
  $token = $tumblr->get_request_token();

  $_SESSION['tumblr_oauth']['account'] = user_load($form['uid']['#value']);
  $_SESSION['tumblr_oauth']['token'] = $token;
  $_SESSION['tumblr_oauth']['destination'] = $_GET['q'];
  drupal_goto($tumblr->get_authorize_url($token));
}

/**
 * Tumblr OAuth callback function, handles if user accepts or deny access.
 *
 * @TODO This code should probably be reviewed.
 *
 * Wrapper to call drupal_form_submit() which wasn't required in D6.
 */
function tumblr_oauth_callback() {

  if (!isset($_GET['oauth_token']) || !isset($_GET['oauth_verifier'])) {
    drupal_set_message(t('User has denied Tumblr App access.'), 'warning', FALSE);
    drupal_goto('<front>');
  }

  $form_state['values']['oauth_token'] = $_GET['oauth_token'];
  $form_state['values']['oauth_verifier'] = $_GET['oauth_verifier'];
  drupal_form_submit('tumblr_oauth_callback_form', $form_state);
}

/**
 * Form builder function. In D6 this form was built in response to the
 * oauth return request from Tumblr, and the setting of
 * $form['#post'] seems to have caused the form to be validated and
 * processed.
 */
function tumblr_oauth_callback_form($form, &$form_state) {
  $form['#post']['oauth_token'] = $_GET['oauth_token'];
  $form['#post']['oauth_verifier'] = $_GET['oauth_verifier'];
  $form['oauth_token'] = array(
    '#type' => 'hidden',
    '#default_value' => $_GET['oauth_token'],
  );
  $form['oauth_verifier'] = array(
    '#type' => 'hidden',
    '#default_value' => $_GET['oauth_verifier'],
  );
  return $form;
}

/**
 * Validate results from Tumblr OAuth return request.
 */
function tumblr_oauth_callback_form_validate($form, &$form_state) {
  $key = variable_get('tumblr_consumer_key', '');
  $secret = variable_get('tumblr_consumer_secret', '');
  $oauth_verifier = '';

  if ($key == '' || $secret == '') {
    form_set_error('', t('Please configure your Tumblr consumer key and secret.'));
  }
  if (isset($_SESSION['tumblr_oauth'])) {
    $form_state['tumblr_oauth'] = $_SESSION['tumblr_oauth']['token'];
    unset($_SESSION['tumblr_oauth']);
  }
  else {
    form_set_error('oauth_token', 'Invalid Tumblr OAuth request: tumblr_oauth was not found in $_SESSION');
  }

  if (isset($form_state['tumblr_oauth'])) {
    $token = $form_state['tumblr_oauth'];
    if (!is_array($token) || !$key || !$secret) {
      form_set_error('oauth_token', t('Invalid Tumblr OAuth request: Token is not an array or consumer variables are missing'));
    }
    if ($token['oauth_token'] != $form_state['values']['oauth_token']) {
      form_set_error('oauth_token', t('Invalid OAuth token: Token does not match stored token'));
    }
  }
  else {
    form_set_error('oauth_token', t('Invalid Tumblr OAuth request: Token data is missing from $form_state'));
  }
  if (isset($form_state['values']['oauth_verifier'])) {
    $oauth_verifier = $form_state['values']['oauth_verifier'];
  }

  module_load_include('lib.php', 'oauth_common');
  module_load_include('lib.php', 'tumblr');
  module_load_include('inc', 'tumblr');

  if ($tumblr = new TumblrOAuth($key, $secret, $token['oauth_token'], $token['oauth_token_secret'])) {
    $response = $tumblr->get_access_token($oauth_verifier);
    if ($response) {
      $form_state['tumblr_oauth']['response'] = $response;
    }
    else {
      form_set_error('oauth_token', t('Invalid Tumblr OAuth request: There was an error getting the access token.'));
    }
  }
  else {
    form_set_error('oauth_token', t('Invalid Tumblr OAuth request: There was an error initiating the TumblrOAuth class'));
  }
}

/**
 * Handle a Tumblr OAuth return request and store the account creds
 * in the DB. Redirects to user/%/edit/tumblr
 *
 * @TODO Redirect better.
 *
 * I don't much like the use of drupal_goto() here as it might
 * interfere with other modules trying to piggyback on the form
 * submission, but setting $form['redirect'] was leaving us at the
 * tumblr/oauth URL.
 */
function tumblr_oauth_callback_form_submit(&$form, &$form_state) {
  global $user;

  $key = variable_get('tumblr_consumer_key', '');
  $secret = variable_get('tumblr_consumer_secret', '');
  $response = $form_state['tumblr_oauth']['response'];
  $values = $form_state['values'];

  $tumblr = new TumblrOAuth($key, $secret, $response['oauth_token'], $response['oauth_token_secret']);

  // Get user info.
  $tumblr_account = $tumblr->user_info();
  $tumblr_account->set_auth($response);

  // Avatar.
  $avatar = NULL;

  // If tumblr user account was retreived.
  if (isset($tumblr_account->name)) {

    // Get avatar.
    $avatar = $tumblr->getBlogAvatar($tumblr_account->name, 64, array('api_key' => $tumblr->api_key));

    // Save Avatar into the files folder.
    if (isset($avatar['avatar_url']) && !empty($avatar['avatar_url'])) {
      $url = $avatar['avatar_url'];
      $directory = file_build_uri('tumblr_files');

      if (!file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
        // If our directory doesn't exist and can't be created, use the default.
        $directory = NULL;
      }
      $avatar = system_retrieve_file($url, $directory, TRUE);
    }
  }

  $account = (isset($values['tumblr_oauth']['account'])? $values['tumblr_oauth']['account'] : $user);

  if (is_array($tumblr_account->blogs) && !empty($tumblr_account->blogs)) {

    foreach ($tumblr_account->blogs as $delta => $blog) {

      // Add avatar to values before creating TumblrBlog class.
      if (isset($avatar) && is_object($avatar) && isset($avatar->uri)) {
        $blog['avatar_url'] = $avatar->uri;
      }
      $tumblr_blog = new TumblrBlog($blog);
      tumblr_blog_save($tumblr_blog, $account);
    }
  }

  // Get Tumbler Posts.
  $posts = $tumblr->getBlogPosts($tumblr_account->name, '', array('api_key' => $tumblr->api_key));

  if (is_array($posts) && !empty($posts)) {
    // Save posts.
    foreach ($posts as $key => $post) {
      tumblr_posts_save($post, $account);
    }
  }

  tumblr_account_save($tumblr_account, TRUE, $account);

  $form['#programmed'] = FALSE;

  $form_state['redirect'] = url('user/' . $account->uid . '/edit/tumblr');
  // redirect isn't firing - because we're using drupal_submit_form()?
  // - so adding drupal_goto() here (but not liking it).
  drupal_goto('user/' . $account->uid . '/edit/tumblr');
}
