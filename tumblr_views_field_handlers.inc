<?php

/**
 * Process Tumblr-style @usernames and URLs before filtering XSS.
 */
class tumblr_views_handler_field_xss extends views_handler_field {
  function option_definition() {
    $conf = TumblrConf::instance();
    $options = parent::option_definition();
    $options['link_urls'] = array('default' => TRUE);
    $options['link_usernames'] = array('default' => TRUE);
    $options['link_hashtags'] = array('default' => FALSE);
    $options['hashtags_url'] = array('default' => 'http://' . $conf->get('search') . '/search?q=%23');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_urls'] = array(
      '#title' => t('Link urls to their destinations'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_urls']),
    );
    $form['link_usernames'] = array(
      '#title' => t('Link Tumblr @usernames to their Tumblr.com urls'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_usernames']),
    );
    $form['link_hashtags'] = array(
      '#title' => t('Link Tumblr #hashtags to another url'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_hashtags']),
    );
    $form['hashtags_url'] = array(
      '#type' => 'textfield',
      '#default_value' => $this->options['hashtags_url'],
      '#process' => array('ctools_dependent_process'),
      '#dependency' => array('edit-options-link-hashtags' => array(TRUE)),
    );
  }

  function render($values) {
    $value = $values->{$this->field_alias};
    if (!empty($this->options['link_urls'])) {
      $filter = new stdClass;
      $filter->settings = array(
        'filter_url_length' => 496,
      );
     $value = _filter_url($value, $filter);
    }
    if (!empty($this->options['link_usernames'])) {
      $conf = TumblrConf::instance();
      $value = _tumblr_filter_text($value, '@', 'http://' . $conf->get('host') . '/');
    } if (!empty($this->options['link_hashtags']) && valid_url($this->options['hashtags_url'])) {
      $value = _tumblr_filter_text($value, '#', url($this->options['hashtags_url']));
    }
    return filter_xss($value);
  }
}

/**
 * Field handler to provide simple renderer that turns a URL into a clickable link.
 */
class tumblr_views_handler_field_profile_image extends views_handler_field {
  function render($values) {
    $value = $values->{$this->field_alias};
    return theme('image', array('path' => $value));
  }
}

/**
 * Field handler to show data of serialized player field.
 */
class tumblr_handler_field_post_photos extends views_handler_field {

  function render($values) {

    // Validate required fields.
    if (!isset($values->tumblr_post_type) || !in_array($values->tumblr_post_type, array('photo'))
      || !isset($values->tumblr_post_id) || empty($values->tumblr_post_id)
      || !isset($values->tumblr_post_uid)) {

      return '';
    }

    // Get post.
    module_load_include('inc', 'tumblr');
    $post = tumblr_post_load($values->tumblr_post_uid, $values->tumblr_post_type, $values->tumblr_post_id);
    $post = is_array($post) ? array_shift($post) : $post;

    // Use theme functions of this module.
    switch ($post->type) {
      case 'photo':
        $theme_type = 'tumblr_post_photo';
        break;
    }

    return theme($theme_type, array('post' => $post));
  }
}

/**
 * Field handler to show data of serialized player field.
 */
class tumblr_handler_field_post_player extends views_handler_field {

  function render($values) {

    // Validate required fields.
    if (!isset($values->tumblr_post_type) || !in_array($values->tumblr_post_type, array('video', 'audio'))
      || !isset($values->tumblr_post_id) || empty($values->tumblr_post_id)
      || !isset($values->tumblr_post_uid)) {

      return '';
    }

    // Get post.
    module_load_include('inc', 'tumblr');
    $post = tumblr_post_load($values->tumblr_post_uid, $values->tumblr_post_type, $values->tumblr_post_id);
    $post = is_array($post) ? array_shift($post) : $post;

    // Use theme functions of this module.
    switch ($post->type) {
      case 'video':
        $theme_type = 'tumblr_post_video';
        break;

      case 'audio':
        $theme_type = 'tumblr_post_audio';
        break;
    }

    return theme($theme_type, array('post' => $post));
  }
}

/**
 * Field handler to show data of serialized player field.
 */
class tumblr_handler_field_post_type extends views_handler_field {

  function render($values) {

    // Get available Tumblr post types.
    $post_types = tumblr_get_posts_types();

    // Remove video, audio and photo, since there are handlers for those types.
    unset($post_types[array_search('audio', $post_types )]);
    unset($post_types[array_search('video', $post_types )]);
    unset($post_types[array_search('photo', $post_types )]);

    // Validate required fields.
    if (!isset($values->tumblr_post_type) || !in_array($values->tumblr_post_type, $post_types)
      || !isset($values->tumblr_post_id) || empty($values->tumblr_post_id)
      || !isset($values->tumblr_post_uid)) {

      return '';
    }

    // Get post.
    module_load_include('inc', 'tumblr');
    $post = tumblr_post_load($values->tumblr_post_uid, $values->tumblr_post_type, $values->tumblr_post_id);
    $post = is_array($post) ? array_shift($post) : $post;

    // Use theme functions of this module.
    switch ($post->type) {
      case 'text':
        $theme_type = 'tumblr_post_text';
        break;

      case 'quote':
        $theme_type = 'tumblr_post_quote';
        break;

      case 'link':
        $theme_type = 'tumblr_post_link';
        break;

      case 'answer':
        $theme_type = 'tumblr_post_answer';
        break;

      case 'chat':
        $theme_type = 'tumblr_post_chat';
        break;
    }

    return theme($theme_type, array('post' => $post));
  }
}

