<?php

/**
 * @file
 * Theming functions for Tumblr module.
 */

/**
 * Theme function for table with Tumblr accounts.
 *
 * @param array $variables
 *  Theming variables; array with the following option:
 *    - form: Drupal's form.
 *
 * @return string
 *   Html table with Tumblr accounts.
 */
function theme_tumblr_account_list_form($variables) {
  $form = $variables['form'];

  if (variable_get('tumblr_import', TRUE) && user_access('import own tumblr posts')) {
    $header = array('', t('Name'), t('Description'), t('Url'), t('Import'), t('Delete'));
  }
  else {
    $header = array('', t('Name'), t('Description'), t('Url'), t('Delete'));
  }

  if (user_access('make tumblr accounts global')) {
    $header[] = '';
  }

  $rows = array();

  foreach (element_children($form['accounts']) as $key) {
    $element = &$form['accounts'][$key];
    if (variable_get('tumblr_import', TRUE) && user_access('import own tumblr posts')) {
      $row = array(
        drupal_render($element['image']),
        drupal_render($element['id']) . drupal_render($element['name']) . drupal_render($element['visible_name']),
        drupal_render($element['description']),
        drupal_render($element['url']),
        drupal_render($element['import']),
        drupal_render($element['delete']),
      );
    }
    else {
      $row = array(
        drupal_render($element['image']),
        drupal_render($element['id']) . drupal_render($element['name']) . drupal_render($element['visible_name']),
        drupal_render($element['description']),
        drupal_render($element['protected']),
        drupal_render($element['delete']),
      );
    }

    if (user_access('make tumblr accounts global')) {
      $label = ($element['#blog']->is_global) ? t('remove global') : t('make global');
      $row[] = l($label, 'user/' . $element['#blog']->uid . '/edit/tumblr/global/' . $element['#blog']->id);
    }

    $rows[] = $row;
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Theming function for Tumblr Posts of text type.
 *
 * @param array $variables
 *  Theming variables; array with the following option:
 *    - post: Object of TumblrPost type.
 *
 * @return string
 *   Html for this type of Tumblr post.
 */
function theme_tumblr_post_text($variables) {
  $post = $variables['post'];

  // Get data from post object and create an array with post information
  // to pass to drupal_render() and create and HTML output.
  $output = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'tumblr-post',
        'post-' . $post->type,
      ),
    ),
  );

  // Post date.
  $date = isset($post->timestamp) && !empty($post->timestamp) ? date(TUMBLR_POST_DATE_FORMAT, $post->timestamp) : '';
  $output['date'] = array(
    '#prefix' => '<div class="post-field post-date">',
    '#markup' => $date,
    '#suffix' => '</div>',
  );

  // Post body, removing photos from body since it is only text type.
  $body = isset($post->body) && !empty($post->body) ? strip_tags($post->body) : '';
  $output['body'] = array(
    '#prefix' => '<div class="post-field post-body">',
    '#markup' => substr($body, 0, TUMBLR_POST_TEXT_MAX_LENGHT) . '...',
    '#suffix' => '</div>',
  );

  // Read on tumblr.
  $link = '';
  if (isset($post->post_url) && !empty($post->post_url)) {
    $link = l(t('Read on Tumblr'), $post->post_url, array('attributes' => array('target' => '_blank')));
  }
  $output['tumblr_link'] = array(
    '#prefix' => '<div class="post-field post-tumblr-link">',
    '#markup' => $link,
    '#suffix' => '</div>',
  );


  return drupal_render($output);
}

/**
 * Theming function for Tumblr Posts of quote type.
 *
 * @param array $variables
 *  Theming variables; array with the following option:
 *    - post: Object of TumblrPost type.
 *
 * @return string
 *   Html for this type of Tumblr post.
 */
function theme_tumblr_post_quote($variables) {
  $post = $variables['post'];

  // Get data from post object and create an array with post information
  // to pass to drupal_render() and create and HTML output.
  $output = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'tumblr-post',
        'post-' . $post->type,
      ),
    ),
  );

  // Post date.
  $date = isset($post->timestamp) && !empty($post->timestamp) ? date(TUMBLR_POST_DATE_FORMAT, $post->timestamp) : '';
  $output['date'] = array(
    '#prefix' => '<div class="post-field post-date">',
    '#markup' => $date,
    '#suffix' => '</div>',
  );

  // Post Source.
  $source = isset($post->source) && !empty($post->source) ? strip_tags($post->source) : '';
  $output['source'] = array(
    '#prefix' => '<div class="post-field post-source">',
    '#markup' => substr($source, 0, TUMBLR_POST_TEXT_MAX_LENGHT) . '...',
    '#suffix' => '</div>',
  );

  // Read on tumblr.
  $link = '';
  if (isset($post->post_url) && !empty($post->post_url)) {
    $link = l(t('Read on Tumblr'), $post->post_url, array('attributes' => array('target' => '_blank')));
  }
  $output['tumblr_link'] = array(
    '#prefix' => '<div class="post-field post-tumblr-link">',
    '#markup' => $link,
    '#suffix' => '</div>',
  );

  return drupal_render($output);
}

/**
 * Theming function for Tumblr Posts of link type.
 *
 * @param array $variables
 *  Theming variables; array with the following option:
 *    - post: Object of TumblrPost type.
 *
 * @return string
 *   Html for this type of Tumblr post.
 */
function theme_tumblr_post_link($variables) {
  $post = $variables['post'];

  // Get data from post object and create an array with post information
  // to pass to drupal_render() and create and HTML output.
  $output = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'tumblr-post',
        'post-' . $post->type,
      ),
    ),
  );

  // Post date.
  $date = isset($post->timestamp) && !empty($post->timestamp) ? date(TUMBLR_POST_DATE_FORMAT, $post->timestamp) : '';
  $output['date'] = array(
    '#prefix' => '<div class="post-field post-date">',
    '#markup' => $date,
    '#suffix' => '</div>',
  );

  // Post Link.
  $link = '';
  if (isset($post->title) && !empty($post->title) && isset($post->url) && !empty($post->url)) {
    $link = l($post->title, $post->url);
  }
  $output['link'] = array(
    '#prefix' => '<div class="post-field post-link">',
    '#markup' => $link,
    '#suffix' => '</div>',
  );

  // Read on tumblr.
  $link = '';
  if (isset($post->post_url) && !empty($post->post_url)) {
    $link = l(t('Read on Tumblr'), $post->post_url, array('attributes' => array('target' => '_blank')));
  }
  $output['tumblr_link'] = array(
    '#prefix' => '<div class="post-field post-tumblr-link">',
    '#markup' => $link,
    '#suffix' => '</div>',
  );

  return drupal_render($output);
}

/**
 * Theming function for Tumblr Posts of answer type.
 *
 * @param array $variables
 *  Theming variables; array with the following option:
 *    - post: Object of TumblrPost type.
 *
 * @return string
 *   Html for this type of Tumblr post.
 */
function theme_tumblr_post_answer($variables) {
  $post = $variables['post'];

  // Get data from post object and create an array with post information
  // to pass to drupal_render() and create and HTML output.
  $output = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'tumblr-post',
        'post-' . $post->type,
      ),
    ),
  );

  // Post date.
  $date = isset($post->timestamp) && !empty($post->timestamp) ? date(TUMBLR_POST_DATE_FORMAT, $post->timestamp) : '';
  $output['date'] = array(
    '#prefix' => '<div class="post-field post-date">',
    '#markup' => $date,
    '#suffix' => '</div>',
  );

  //TODO: Implement here, content of this post.

  // Read on tumblr.
  $link = '';
  if (isset($post->post_url) && !empty($post->post_url)) {
    $link = l(t('Read on Tumblr'), $post->post_url, array('attributes' => array('target' => '_blank')));
  }
  $output['tumblr_link'] = array(
    '#prefix' => '<div class="post-field post-tumblr-link">',
    '#markup' => $link,
    '#suffix' => '</div>',
  );

  return drupal_render($output);
}

/**
 * Theming function for Tumblr Posts of video type.
 *
 * @param array $variables
 *  Theming variables; array with the following option:
 *    - post: Object of TumblrPost type.
 *
 * @return string
 *   Html for this type of Tumblr post.
 */
function theme_tumblr_post_video($variables) {
  $post = $variables['post'];

  // Get data from post object and create an array with post information
  // to pass to drupal_render() and create and HTML output.
  $output = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'tumblr-post',
        'post-' . $post->type,
      ),
    ),
  );

  // Post Video.
  $video = '';

  if (isset($post->player) && !empty($post->player)) {
    $videos = unserialize($post->player);
    if (is_array($videos)) {
      $video = array_shift($videos);
      $video = $video['embed_code'];
    }
  }

  $output['video'] = array(
    '#prefix' => '<div class="post-field post-video">',
    '#markup' => $video,
    '#suffix' => '</div>',
  );

  $output['right_prefix'] = array(
    '#markup' => '<div class="tumblr-post-right-content">',
  );

  // Post date.
  $date = isset($post->timestamp) && !empty($post->timestamp) ? date(TUMBLR_POST_DATE_FORMAT, $post->timestamp) : '';
  $output['date'] = array(
    '#prefix' => '<div class="post-field post-date">',
    '#markup' => $date,
    '#suffix' => '</div>',
  );

  // Post caption.
  $text = isset($post->caption) && !empty($post->caption) ? strip_tags($post->caption) : '';
  $output['caption'] = array(
    '#prefix' => '<div class="post-field post-caption">',
    '#markup' => substr($text, 0, TUMBLR_POST_TEXT_MAX_LENGHT) . '...',
    '#suffix' => '</div>',
  );

  // Read on tumblr.
  $link = '';
  if (isset($post->post_url) && !empty($post->post_url)) {
    $link = l(t('Read on Tumblr'), $post->post_url, array('attributes' => array('target' => '_blank')));
  }
  $output['tumblr_link'] = array(
    '#prefix' => '<div class="post-field post-tumblr-link">',
    '#markup' => $link,
    '#suffix' => '</div>',
  );

  $output['right_suffix'] = array(
    '#markup' => '</div>',
  );

  return drupal_render($output);
}

/**
 * Theming function for Tumblr Posts of audio type.
 *
 * @param array $variables
 *  Theming variables; array with the following option:
 *    - post: Object of TumblrPost type.
 *
 * @return string
 *   Html for this type of Tumblr post.
 */
function theme_tumblr_post_audio($variables) {
  $post = $variables['post'];

  // Get data from post object and create an array with post information
  // to pass to drupal_render() and create and HTML output.
  $output = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'tumblr-post',
        'post-' . $post->type,
      ),
    ),
  );

  // Post Audio.
  $video = '';

  if (isset($post->player) && !empty($post->player)) {
    $videos = unserialize($post->player);
    if (is_array($videos)) {
      $video = array_shift($videos);
      $video = $video['embed_code'];
    }
  }

  // Post Audio.
  $audio = '';
  if (isset($post->player) && !empty($post->player)) {
    $audio = unserialize($post->player);
    if (is_array($audio)) {
      $audio = array_shift($audio);
    }
  }

  $output['audio'] = array(
    '#prefix' => '<div class="post-field post-audio">',
    '#markup' => $audio,
    '#suffix' => '</div>',
  );

  // Post date.
  $date = isset($post->timestamp) && !empty($post->timestamp) ? date(TUMBLR_POST_DATE_FORMAT, $post->timestamp) : '';
  $output['date'] = array(
    '#prefix' => '<div class="post-field post-date">',
    '#markup' => $date,
    '#suffix' => '</div>',
  );

  $output['right_prefix'] = array(
    '#markup' => '<div class="tumblr-post-right-content">',
  );

  // Post caption.
  $text = isset($post->caption) && !empty($post->caption) ? strip_tags($post->caption) : '';
  $output['caption'] = array(
    '#prefix' => '<div class="post-field post-caption">',
    '#markup' => substr($text, 0, TUMBLR_POST_TEXT_MAX_LENGHT) . '...',
    '#suffix' => '</div>',
  );

  // Read on tumblr.
  $link = '';
  if (isset($post->post_url) && !empty($post->post_url)) {
    $link = l(t('Read on Tumblr'), $post->post_url, array('attributes' => array('target' => '_blank')));
  }
  $output['tumblr_link'] = array(
    '#prefix' => '<div class="post-field post-tumblr-link">',
    '#markup' => $link,
    '#suffix' => '</div>',
  );

  $output['right_suffix'] = array(
    '#markup' => '</div>',
  );

  return drupal_render($output);
}

/**
 * Theming function for Tumblr Posts of photo type.
 *
 * @param array $variables
 *  Theming variables; array with the following option:
 *    - post: Object of TumblrPost type.
 *
 * @return string
 *   Html for this type of Tumblr post.
 */
function theme_tumblr_post_photo($variables) {
  $post = $variables['post'];

  // Get data from post object and create an array with post information
  // to pass to drupal_render() and create and HTML output.
  $output = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'tumblr-post',
        'post-' . $post->type,
      ),
    ),
  );

  // Post Photos.
  $photos= '';
  if (isset($post->photos) && !empty($post->photos)) {
    $photos = unserialize($post->photos);
    if (is_array($photos)) {
      // Get all photos.
      foreach ($photos as $key => $photo) {
        $image = variable_get('tumblr_image_style') ? theme('image_style', array('image_style' => variable_get('tumblr_image_style'), 'path' => $photo['original_size']['url'])) : theme('image', array('path' => $photo['original_size']['url']));
        $output['photos_' . $key] = array(
          '#prefix' => '<div class="post-field post-photo">',
          '#markup' => $image,
          '#suffix' => '</div>',
        );
      }
    }
  }

  $output['right_prefix'] = array(
    '#markup' => '<div class="tumblr-post-right-content">',
  );

  // Post date.
  $date = isset($post->timestamp) && !empty($post->timestamp) ? date(TUMBLR_POST_DATE_FORMAT, $post->timestamp) : '';
  $output['date'] = array(
    '#prefix' => '<div class="post-field post-date">',
    '#markup' => $date,
    '#suffix' => '</div>',
  );

  // Post caption.
  $text = isset($post->caption) && !empty($post->caption) ? strip_tags($post->caption) : '';
  $output['caption'] = array(
    '#prefix' => '<div class="post-field post-caption">',
    '#markup' => substr($text, 0, TUMBLR_POST_TEXT_MAX_LENGHT) . '...',
    '#suffix' => '</div>',
  );

  // Read on tumblr.
  $link = '';
  if (isset($post->post_url) && !empty($post->post_url)) {
    $link = l(t('Read on Tumblr'), $post->post_url, array('attributes' => array('target' => '_blank')));
  }
  $output['tumblr_link'] = array(
    '#prefix' => '<div class="post-field post-tumblr-link">',
    '#markup' => $link,
    '#suffix' => '</div>',
  );

  $output['right_suffix'] = array(
    '#markup' => '</div>',
  );

  return drupal_render($output);
}

/**
 * Theming function for Tumblr Posts of chat type.
 *
 * @param array $variables
 *  Theming variables; array with the following option:
 *    - post: Object of TumblrPost type.
 *
 * @return string
 *   Html for this type of Tumblr post.
 */
function theme_tumblr_post_chat($variables) {
  $post = $variables['post'];

  // Get data from post object and create an array with post information
  // to pass to drupal_render() and create and HTML output.
  $output = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'tumblr-post',
        'post-' . $post->type,
      ),
    ),
  );

  // Post date.
  $date = isset($post->timestamp) && !empty($post->timestamp) ? date(TUMBLR_POST_DATE_FORMAT, $post->timestamp) : '';
  $output['date'] = array(
    '#prefix' => '<div class="post-field post-date">',
    '#markup' => $date,
    '#suffix' => '</div>',
  );

  // Post body.
  $body = isset($post->body) && !empty($post->body) ? strip_tags($post->body) : '';
  $output['body'] = array(
    '#prefix' => '<div class="post-field post-body">',
    '#markup' => substr($body, 0, TUMBLR_POST_TEXT_MAX_LENGHT) . '...',
    '#suffix' => '</div>',
  );

  // Read on tumblr.
  $link = '';
  if (isset($post->post_url) && !empty($post->post_url)) {
    $link = l(t('Read on Tumblr'), $post->post_url, array('attributes' => array('target' => '_blank')));
  }
  $output['tumblr_link'] = array(
    '#prefix' => '<div class="post-field post-tumblr-link">',
    '#markup' => $link,
    '#suffix' => '</div>',
  );

  return drupal_render($output);
}
