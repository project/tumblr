<?php

/**
 * @file
 * Install, update and uninstall functions for the tumblr module.
 *
 */

/**
 * Implements hook_schema().
 */
function tumblr_schema() {
  $schema['tumblr_account'] = array(
    'description' => "Stores information on specific Tumblr user accounts.",
    'fields' => array(
      'id' => array(
        'description' => "The unique identifier of the {tumblr_account}.",
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => "The {users}.uid of the owner of this account",
        'type' => 'int',
        'unsigned' => TRUE,
        'size' => 'big',
        'not null' => FALSE,
      ),
      'name' => array(
        'description' => 'Short name of the blog',
        'type' => 'varchar',
        'length' => 255,
      ),
      'default_post_format' => array(
        'description' => 'The host for this account can be a laconi.ca instance',
        'type' => 'varchar',
        'length' => 255,
      ),
      'likes' => array(
        'description' => "The total count of the user's likes",
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'blogs' => array(
        'description' => "Array of blogs the user has permission to post to",
        'type' => 'text',
        'not null' => FALSE,
        'serialize' => TRUE,
      ),
      'oauth_token' => array(
        'description' => 'The token_key for oauth-based access.',
        'type' => 'varchar',
        'length' => 64,
      ),
      'oauth_token_secret' => array(
        'description' => 'The token_secret for oauth-based access.',
        'type' => 'varchar',
        'length' => 64,
      ),
    ),
    'indexes' => array('name_uid_account_id' => array('name', 'uid', 'id')),
    'unique keys' => array(
      'name_uid' => array('name', 'uid'),
    ),
    'primary key' => array('id'),
  );

  $schema['tumblr_blog'] = array(
    'description' => "Stores information on specific Tumblr user blogs.",
    'fields' => array(
      'id' => array(
        'description' => "The unique identifier of the {tumblr_blog}.",
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => "The {users}.uid of the owner of this blog",
        'type' => 'int',
        'unsigned' => TRUE,
        'size' => 'big',
        'not null' => FALSE,
      ),
      'title' => array(
        'description' => 'Short name of the blog',
        'type' => 'varchar',
        'length' => 255,
      ),
      'name' => array(
        'description' => 'Short name of the blog',
        'type' => 'varchar',
        'length' => 255,
      ),
      'posts' => array(
        'description' => "The total count of posts on this blog",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'url' => array(
        'description' => 'Short name of the blog',
        'type' => 'varchar',
        'length' => 255,
      ),
      'updated' => array(
        'description' => "The time of the most recent post, in seconds since the epoch.",
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'description' => array(
        'description' => 'The blog\'s description',
        'type' => 'text',
        'serialized' => TRUE,
        'not null' => FALSE,
      ),
      'ask' => array(
        'description' => "Indicates whether the blog allows questions",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'ask_anon' => array(
        'description' => "Indicates whether the blog allows anonymous questions. Returned only if ask is true",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'is_nsfw' => array(
        'description' => "Indicates whether the blog is not safe for work. Returned only if is_nsfw is true",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'followed' => array(
        'description' => "Indicates if the blog has followers",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'can_send_fan_mail' => array(
        'description' => "Indicates if the blog can send fan mail",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'share_likes' => array(
        'description' => "Indicates if the blog shares likes",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'likes' => array(
        'description' => "Indicates the number of likes",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'twitter_enabled' => array(
        'description' => "Indicates if Twitter is enabled",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'twitter_send' => array(
        'description' => "Indicates if posts are tweeted",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'facebook_open_graph_enabled' => array(
        'description' => "Indicates if the blog uses FB Open Graph",
        'type' => 'varchar',
        'length' => 1,
        'not null' => FALSE,
        'default' => 'N',
      ),
      'tweet' => array(
        'description' => "Indicates if posts are tweeted",
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
      ),
      'facebook' => array(
        'description' => "Indicates if the blog uses FB",
        'type' => 'varchar',
        'length' => 1,
        'not null' => FALSE,
        'default' => 'N',
      ),
      'avatar_url' => array(
        'description' => 'The URL of the avatar image. the response points to the avatar image. That means you can embed this method in an img tag in HTML.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'oauth_token' => array(
        'description' => 'The token_key for oauth-based access.',
        'type' => 'varchar',
        'length' => 64,
      ),
      'oauth_token_secret' => array(
        'description' => 'The token_secret for oauth-based access.',
        'type' => 'varchar',
        'length' => 64,
      ),
      'import' => array(
        'description' => "Boolean flag indicating whether the {tumblr_blog}'s posts should be pulled in by the site.",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 1,
      ),
      'last_refresh' => array(
        'description' => "A UNIX timestamp marking the date Tumblr statuses were last fetched on.",
        'type' => 'int',
        'not null' => FALSE,
        'default' => 0,
      ),
      'is_global' => array(
        'description' => "Boolean flag indicating if this account is available for global use",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
    ),
    'indexes' => array('name_uid_blog_id' => array('name', 'uid', 'id')),
    'unique keys' => array(
      'name_uid' => array('name', 'uid'),
    ),
    'primary key' => array('id'),
  );

  $schema['tumblr_post'] = array(
    'description' => "Stores individual Tumblr posts.",
    'fields' => array(
      'id' => array(
        'description' => "The post's unique ID.",
        'type' => 'numeric',
        'unsigned' => TRUE,
        'precision' => 20,
        'scale' => 0,
        'not null' => FALSE,
        'default' => 0,
      ),
      'uid' => array(
        'description' => "The {users}.uid of the owner of this account",
        'type' => 'int',
        'unsigned' => TRUE,
        'size' => 'big',
        'not null' => FALSE,
      ),
      'blog_name' => array(
        'description' => "Blog name. Links to the {tumblr_account} record.",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'post_url' => array(
        'description' => "The location of the {tumblr_post} post.",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'slug' => array(
        'description' => "The slug of the {tumblr_post} post.",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'type' => array(
        'description' => "The type of the {tumblr_post} post.",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'date' => array(
        'description' => "The GMT date and time of the post, as a string.",
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'timestamp' => array(
        'description' => "The time of the post, in seconds since the epoch",
        'type' => 'int',
        'not null' => FALSE,
      ),
      'state' => array(
        'description' => "The post state: published or unpublished",
        'type' => 'varchar',
        'length' => 64,
        'not null' => FALSE,
      ),
      'format' => array(
        'description' => "The post format: html or markdown",
        'type' => 'varchar',
        'length' => 64,
        'not null' => FALSE,
      ),
      'reblog_key' => array(
        'description' => "The key used to reblog this post",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'tags' => array(
        'description' => "Tags applied to the post.",
        'type' => 'varchar',
        'serialized' => TRUE,
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'short_url' => array(
        'description' => "The short url",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'highlighted' => array(
        'description' => "I don't know",
        'type' => 'varchar',
        'serialized' => TRUE,
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'caption' => array(
        'description' => "Caption used with photo types",
        'type' => 'text',
        'not null' => FALSE,
      ),
      'mobile' => array(
        'description' => "Indicates whether the post was created via mobile or email publishing.",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'feed_item' => array(
        'description' => "The URL for the source of the content (for quotes, reblogs, etc.)",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'from_feed_id' => array(
        'description' => "The title of the source site",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'title' => array(
        'description' => "The optional title of the post.",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'body' => array(
        'description' => "The full post body",
        'type' => 'text',
        'not null' => FALSE,
      ),
      'photos' => array(
        'description' => "Photos information",
        'type' => 'text',
        'serialized' => TRUE,
        'not null' => FALSE,
      ),
      'player' => array(
        'description' => "Player information",
        'type' => 'text',
        'serialized' => TRUE,
        'not null' => FALSE,
      ),
      'source' => array(
        'description' => "Post source.",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'source_url' => array(
        'description' => "Post source URL.",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'source_title' => array(
        'description' => "Post source tile.",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'text' => array(
        'description' => "Post text.",
        'type' => 'text',
        'not null' => FALSE,
      ),
      'url' => array(
        'description' => "Post url.",
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
    'indexes' => array(
      'blog_name' => array('blog_name'),
      'type' => array('type'),
      'blog_name_uid_post_id' => array('blog_name', 'uid', 'id'),
      'uid_type' => array('uid', 'type'),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function tumblr_install() {
  // Set the weight to 3, making it heavier than Pathauto.
  db_update('system')
    ->fields(array(
      'weight' => 3,
    ))
    ->condition('type', 'module')
    ->condition('name', 'tumblr')
    ->execute();
}

/**
 * Implements hook_uninstall().
 */
function tumblr_uninstall() {
  // Remove variables
  variable_del('tumblr_import');
  variable_del('tumblr_expire');
  variable_del('tumblr_consumer_key');
  variable_del('tumblr_consumer_secret');
  variable_del('tumblr_post_types');
  variable_del('tumblr_host');
  variable_del('tumblr_post_default_format');
  variable_del('tumblr_signin_button');
  variable_del('tumblr_signin_register');
}

/**
 * Implements hook_update_last_removed().
 */
function tumblr_update_last_removed() {
  return 6005;
}

/**
 * Implementations of hook_update_N().
 */

/**
 * Fix the length of the {tumblr_post}.text field.
 */
function tumblr_update_7100() {
  $table = 'tumblr_post';
  $field = 'text';
  $spec = array(
    'description' => "Post text.",
    'type' => 'text',
    'not null' => FALSE,
  );
  db_change_field($table, $field, $field, $spec);
}
